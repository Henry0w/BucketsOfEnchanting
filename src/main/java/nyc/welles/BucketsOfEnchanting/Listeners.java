package nyc.welles.BucketsOfEnchanting;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Listeners implements Listener {
    public static boolean getIfEnoughExp(PlayerInteractEvent playerInteractEvent){
        ItemStack mainItem = playerInteractEvent.getPlayer().getInventory().getItemInMainHand();
        if (playerInteractEvent.getClickedBlock() != null || playerInteractEvent.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (playerInteractEvent.getClickedBlock().getType() == Material.EMERALD_BLOCK) {
                if (mainItem.getType() == Material.BUCKET) {
                    Player player = playerInteractEvent.getPlayer();
                    if (player.getLevel() >= 25) {
                        player.giveExp(-909);
                    } else{

                        return false;
                    }
                    mainItem.setType(Material.MILK_BUCKET);
                    ItemMeta mainMeta = mainItem.getItemMeta();
                    mainMeta.setDisplayName("Bucket O' Enchanting");
                    mainItem.setItemMeta(mainMeta);
                    mainItem.addUnsafeEnchantment(Enchantment.DURABILITY, 1);
                    playerInteractEvent.getPlayer().getInventory().setItemInMainHand(null);
                    playerInteractEvent.getPlayer().getInventory().setItemInMainHand(mainItem);

                }
            }
        }
        return true;
    }
    @EventHandler(priority = EventPriority.HIGH)
    public void onRightClick(PlayerInteractEvent playerInteractEvent) {
        Player player = playerInteractEvent.getPlayer();
        if (!getIfEnoughExp(playerInteractEvent)){
            for (int i = 0; i < 15; i++){
                player.sendMessage(" ");
            }
            player.sendMessage("+++++++++++++++++++++++++++++++++++++++++");
            player.sendMessage("You don't have enough xp yet!");
            player.sendMessage("+++++++++++++++++++++++++++++++++++++++++");

            for (int i = 0; i < 3; i++){
                player.sendMessage(" ");
            }
        }


    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onPlayerConsume(PlayerItemConsumeEvent playerItemConsumeEvent) {
        if (playerItemConsumeEvent.getItem().getType() == Material.MILK_BUCKET) {
            if (playerItemConsumeEvent.getItem().containsEnchantment(Enchantment.DURABILITY)) {
                playerItemConsumeEvent.getPlayer().giveExp(910);
            }
        }
    }
}
